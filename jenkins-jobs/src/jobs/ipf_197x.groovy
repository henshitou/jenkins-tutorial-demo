pipelineJob('seed-pipeline-job(197x)') {

    def repo = 'https://svn-01.ta.philips.com/svn/icap-platform/trunk'

    description("seed-pipeline-job(197x)")
    keepDependencies(false)

    definition {

        cpsScm {
            scm {
                svn {
                    location('https://svn-01.ta.philips.com/svn/icap-platform/trunk')
                    //credentials('5f139fb7-9e46-44a7-ac1e-fd010ec54cc4')
                }
            }

        }
    }
}